const INITIAL_STATE = {
    list: []
}

export function todolist(state=INITIAL_STATE, action){
    switch (action.type) {
        case 'ADD':
            return {
                list: [...state.list, action.payload]}
        case 'DELETE':
            const newAarry = [...state.list]
            newAarry.splice(action.payload,1)
            return{
                list: newAarry
            }
        case "COMPLETE":
            const newAarry2 = [...state.list]
                newAarry2[action.payload].marcado = !newAarry2[action.payload].marcado
            return {
                list: newAarry2}
        case "EDIT":
            const newAarry3 = [...state.list]
            newAarry3[action.payload.index].tarea = action.payload.value
            return {
                list: newAarry3}
        default:
            return state
    }
}
