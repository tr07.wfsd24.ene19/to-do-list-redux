import React ,{Component} from 'react';
import Form from './Components/Form';
import './App.css';
import {connect} from 'react-redux'
import TaskList from "./Components/TaskList";

class App extends Component  {

    constructor(props) {
        super(props);
    }

    render () {

        console.log(this.props.list)
        //ver si hay tareas
        const parrafo = Object.keys(this.props.list).length === 0 ? <p>no hay tareas</p> : '';

        return (
            <div className="container p-5">
                <h1 className="mb-4 text-secondary">To do list con Redux</h1>
                <Form/>
                <h2 className="mt-4 mb-4">Listado de Tareas</h2>
                {parrafo}
                <div className="row justify-content-start">
                    <div className="col-md-8">
                        <ul className="list-group">
                            {this.props.list.map((tarea, index) => (
                                <TaskList key={index} clave={index} tarea={tarea.tarea} marcado={tarea.marcado}/>
                                )
                            )}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {list: state.list}
}

export default connect(mapStateToProps)(App)
