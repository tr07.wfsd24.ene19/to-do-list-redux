import React,{Component}  from 'react';
import {connect} from "react-redux";
import Swal from 'sweetalert2'

class TaskList extends Component {

    cogerIndex = (index,e) =>{

        //coge el valor del task desde el id del botton
        const value = e.getAttribute('value')

        if (e.getAttribute('data-id')==='delete') {

            //disparo para la store con delete
            this.props.delete(index)

        } else {

            this.editTask(value,index)
        }
    }

    async editTask (value,index) {
        const {value: task} = await Swal.fire({
            title: 'Editar tarea',
            input: 'text',
            inputValue: value,
            showCancelButton: true,
            inputValidator: (value) => {
                if (!value) {
                    return 'No dejes el campo vacio'
                }
            }
        })

        const object = {
            index: index,
            value: task
        }

        if (task) {
            //disparo para la store con edit
            this.props.edit(object)
        }
    }

    completeTask = (index) =>{

    //disparo para la store para complete
        this.props.complete(index)
    }

    render() {
        let marca ;
        if (this.props.marcado) {
            marca = "list-group-item-secondary"
        }

        return (
            <div className="flex-row d-flex justify-content-start">
                <div className="col pl-0">
                    <li onClick={() => this.completeTask(this.props.clave)}
                        className={`list-group-item mt-1 mb-1 p-2 ${marca}`} style={{
                        color: this.props.marcado ? "red" : 'black',
                        textDecoration: this.props.marcado ? 'line-through' : 'none'
                    }}>{this.props.tarea}</li>
                </div>
                <div className="col d-flex align-items-center">
                    <button data-id="edit" value={this.props.tarea} onClick={(e) => this.cogerIndex(this.props.clave,e.currentTarget,e.currentTarget)} type="button"
                            className="btn btn-warning pt-1 mr-1" disabled={this.props.marcado}>Editar</button>
                    <button data-id="delete" onClick={(e) => this.cogerIndex(this.props.clave,e.currentTarget)} type="button"
                            className="btn btn-danger pt-1">Eliminar</button>
                </div>

            </div>
        );
    }
}
const mapDispatchToProps = dispatch => {
    return {
        delete: (index) => dispatch({type:'DELETE',payload: index}),
        complete: (index) => dispatch({type:'COMPLETE',payload: index}),
        edit: (object) => dispatch({type:'EDIT',payload: object}),
    }
}

export default connect(null, mapDispatchToProps)(TaskList)
