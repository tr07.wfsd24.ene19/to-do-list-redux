import React,{Component}  from 'react';
import {connect} from 'react-redux'

class Form extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            tarea: "",
            marcado:false,
        }
    }

    //setear estado
    cogerDato = (e) => {
        this.setState({tarea:e})

    }

    //submit
    sendStore =(e) => {

        e.preventDefault();

        if (this.state.tarea !== "") {

            //disparo para la store con add
            this.props.add(this.state)

            //borrar la barra
            this.setState({tarea: ""})
        }
    }

    render() {
        return (
            <form onSubmit={this.sendStore}>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput1"><h2>Añade Tarea</h2></label>
                    <input value={this.state.tarea} onChange={e=>this.cogerDato(e.target.value)} type="text" className="form-control" id="exampleFormControlInput1"
                           placeholder="Hacer la compra, limpiar..." required/>
                </div>
                <button type="submit" className="btn btn-primary mb-2">Añadir</button>
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: (state) => dispatch({type:'ADD',payload: state}),
    }
}

export default connect(null, mapDispatchToProps)(Form)
